package com.ubt.frontend.managedrugs.models;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.models.BoBaseModel;
import com.ubt.frontend.staticdata.categories.models.CategoryModel;
import com.ubt.frontend.staticdata.classifications.models.ClassificationModel;
import com.ubt.frontend.staticdata.sideeffects.models.SideEffectModel;

import java.util.List;

public class DrugModel extends BoBaseModel implements AutoCompletable {
  private String name;
  private Long classificationId;
  private Long categoryId;
  private List<Long> sideEffectIds;
  /**
   * Sub object fields
   */
  private ClassificationModel classification;
  private CategoryModel category;
  private List<SideEffectModel> sideEffects;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getClassificationId() {
    return classificationId;
  }

  public void setClassificationId(Long classificationId) {
    this.classificationId = classificationId;
  }

  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }

  public List<Long> getSideEffectIds() {
    return sideEffectIds;
  }

  public void setSideEffectIds(List<Long> sideEffectIds) {
    this.sideEffectIds = sideEffectIds;
  }

  public ClassificationModel getClassification() {
    return classification;
  }

  public void setClassification(ClassificationModel classification) {
    this.classification = classification;
  }

  public CategoryModel getCategory() {
    return category;
  }

  public void setCategory(CategoryModel category) {
    this.category = category;
  }

  public List<SideEffectModel> getSideEffects() {
    return sideEffects;
  }

  public void setSideEffects(List<SideEffectModel> sideEffects) {
    this.sideEffects = sideEffects;
  }

  @Override
  public Object getAutocompleteId() {
    return id;
  }

  @Override
  public String getText() {
    return name;
  }
}
