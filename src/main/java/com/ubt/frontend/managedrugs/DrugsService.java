package com.ubt.frontend.managedrugs;

import com.ubt.frontend.managedrugs.models.DrugModel;
import com.ubt.frontend.staticdata.sideeffects.models.SideEffectModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DrugsService {
  Page<DrugModel> getDrugs(Pageable pageable, String name, Long classificationId, Long categoryId, List<String> expand);

  DrugModel getDrugById(long drugId);

  DrugModel createDrug(DrugModel drugModel);

  DrugModel updateDrug(long drugId, DrugModel drugModel);

  DrugModel deleteDrug(long drugId);

  List<SideEffectModel> getSideEffectsByDrugsIds(List<Long> drugsIds);

  void addDrugSideEffects(long drugId, List<Long> sideEffectIds);
}
