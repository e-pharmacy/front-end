package com.ubt.frontend.managedrugs;

import com.ubt.frontend.clients.MedicineFeignClient;
import com.ubt.frontend.managedrugs.models.DrugModel;
import com.ubt.frontend.staticdata.sideeffects.models.SideEffectModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DrugsServiceImpl implements DrugsService {
  private final MedicineFeignClient medicineFeignClient;

  @Autowired
  public DrugsServiceImpl(MedicineFeignClient medicineFeignClient) {
    this.medicineFeignClient = medicineFeignClient;
  }

  @Override
  public Page<DrugModel> getDrugs(Pageable pageable, String name, Long classificationId, Long categoryId, List<String> expand) {
    return medicineFeignClient.getDrugs(pageable, name, classificationId, categoryId, expand);
  }

  @Override
  public DrugModel getDrugById(long drugId) {
    return medicineFeignClient.getDrugById(drugId);
  }

  @Override
  public DrugModel createDrug(DrugModel drugModel) {
    return medicineFeignClient.createDrug(drugModel);
  }

  @Override
  public DrugModel updateDrug(long drugId, DrugModel drugModel) {
    return medicineFeignClient.updateDrug(drugId, drugModel);
  }

  @Override
  public DrugModel deleteDrug(long drugId) {
    return medicineFeignClient.deleteDrug(drugId);
  }

  @Override
  public List<SideEffectModel> getSideEffectsByDrugsIds(List<Long> drugsIds) {
    return medicineFeignClient.getSideEffectsByDrugsIds(Pageable.unpaged(), drugsIds).getContent();
  }

  @Override
  public void addDrugSideEffects(long drugId, List<Long> sideEffectIds) {
    medicineFeignClient.addDrugSideEffects(drugId, sideEffectIds);
  }
}
