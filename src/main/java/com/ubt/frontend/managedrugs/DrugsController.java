package com.ubt.frontend.managedrugs;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.autocomplete.AutocompleteService;
import com.ubt.frontend.managedrugs.models.DrugModel;
import com.ubt.frontend.staticdata.categories.models.CategoryModel;
import com.ubt.frontend.staticdata.sideeffects.models.SideEffectModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/drugs")
public class DrugsController {
  private final DrugsService drugsService;

  @Autowired
  public DrugsController(DrugsService drugsService) {
    this.drugsService = drugsService;
  }

  @ResponseBody
  @GetMapping(value = "/autocomplete")
  public List<AutoCompletable> autocompleteDrugs(Pageable pageable, @RequestParam(value = "q", required = false) String query) {
    List<DrugModel> drugs = drugsService.getDrugs(pageable, query, null, null, null).getContent();
    return AutocompleteService.autocomplete(drugs, query);
  }

  @GetMapping(value = "")
  public String serveDrugsOverview(Model model, Pageable pageable) {
    Page<DrugModel> drugsPage = drugsService.getDrugs(pageable, null, null, null, List.of("all"));

    model.addAttribute("drugsPage", drugsPage);

    return "drugs/drugs_listview";
  }

  @GetMapping(value = "/elements")
  public String serveDrugsListElements(@RequestParam(required = false) String name, @RequestParam(required = false) Long classificationId, @RequestParam(required = false) Long categoryId, Model model, Pageable pageable) {
    Page<DrugModel> drugsPage = drugsService.getDrugs(pageable, name, classificationId, categoryId, List.of("all"));

    model.addAttribute("drugsPage", drugsPage);

    return "drugs/fragments/drugs_table";
  }

  @GetMapping(value = "/new")
  public String serveDrugNew(Model model) {
    model.addAttribute("drugModel", new DrugModel());
    model.addAttribute("sideEffectIds", new ArrayList<Long>());

    model.addAttribute("postUrl", "/drugs");
    return "drugs/edit_drug";
  }

  @GetMapping(value = "/{id}")
  public String serveDrugEdit(Model model, @PathVariable long id) {
    DrugModel drugModel = drugsService.getDrugById(id);
    List<SideEffectModel> sideEffects = drugsService.getSideEffectsByDrugsIds(List.of(id));

    model.addAttribute("drugModel", drugModel);

    //Set external objects to fill dropdowns
    model.addAttribute("categories", List.of(drugModel.getCategory()));
    model.addAttribute("classifications", List.of(drugModel.getClassification()));
    model.addAttribute("sideEffects", sideEffects);
    model.addAttribute("sideEffectIds", sideEffects.stream().map(SideEffectModel::getId).collect(Collectors.toList()));

    //Set the url to submit the form
    model.addAttribute("postUrl", "/drugs/" + id);
    return "drugs/edit_drug";
  }

  @PostMapping(value = "")
  public String createDrug(@ModelAttribute DrugModel drugModel) {
    DrugModel createdDrug = drugsService.createDrug(drugModel);

    drugsService.addDrugSideEffects(createdDrug.getId(), drugModel.getSideEffectIds());
    return "redirect:/drugs";
  }

  @PostMapping(value = "/{id}")
  public String updateDrug(@PathVariable long id, @ModelAttribute DrugModel drugModel) {
    drugsService.updateDrug(id, drugModel);

    drugsService.addDrugSideEffects(id, drugModel.getSideEffectIds());
    return "redirect:/drugs";
  }

  @DeleteMapping(value = "/{id}")
  public String deleteDrug(@PathVariable long id) {
    drugsService.deleteDrug(id);

    return "redirect:/drugs";
  }
}
