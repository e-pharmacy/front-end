package com.ubt.frontend.notifications;

import com.ubt.frontend.notifications.models.NotificationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/notifications")
public class NotificationsController {
  private final NotificationsService notificationsService;

  @Autowired
  public NotificationsController(NotificationsService notificationsService) {
    this.notificationsService = notificationsService;
  }

  @GetMapping(value = "/dropdown")
  public String serveNotificationsList(Model model, Pageable pageable) {
    Page<NotificationModel> notificationsPage = notificationsService.getNotificationByUserId(pageable, 1L);

    model.addAttribute("notificationsPage", notificationsPage);

    return "commons/fragments/notifications_list";
  }

  @GetMapping(value = "")
  public String serveAllNotificationsList(Model model) {
    Page<NotificationModel> notificationsPage = notificationsService.getNotificationByUserId(Pageable.unpaged(), 1L);

    model.addAttribute("notificationsPage", notificationsPage);

    return "notifications/notifications_listview";
  }
}
