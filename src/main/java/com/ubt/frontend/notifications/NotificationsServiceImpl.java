package com.ubt.frontend.notifications;

import com.ubt.frontend.clients.NotificationsFeignClient;
import com.ubt.frontend.commons.notifications.NotificationsDataResolver;
import com.ubt.frontend.notifications.models.NotificationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationsServiceImpl implements NotificationsService {
  private final NotificationsFeignClient notificationsFeignClient;

  @Autowired
  public NotificationsServiceImpl(NotificationsFeignClient notificationsFeignClient) {
    this.notificationsFeignClient = notificationsFeignClient;
  }

  @Override
  public Page<NotificationModel> getNotificationByUserId(Pageable pageable, long userId) {
    Page<NotificationModel> notificationsPage = notificationsFeignClient.getNotificationByUserId(pageable, userId);
    List<NotificationModel> notifications = notificationsPage.getContent();
    NotificationsDataResolver.resolveData(notifications);

    return new PageImpl<>(notifications, pageable, notificationsPage.getTotalElements());
  }
}
