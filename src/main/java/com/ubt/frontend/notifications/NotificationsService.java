package com.ubt.frontend.notifications;

import com.ubt.frontend.notifications.models.NotificationModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface NotificationsService {
  Page<NotificationModel> getNotificationByUserId(Pageable pageable, long userId);
}
