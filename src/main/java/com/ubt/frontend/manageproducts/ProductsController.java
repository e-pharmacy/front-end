/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.frontend.manageproducts;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.autocomplete.AutocompleteService;
import com.ubt.frontend.manageproducts.models.ProductModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/products")
public class ProductsController {
  private final ProductsService productsService;
  @Value("${feign.address.medicine-service}")
  private String medicineServiceUrl;

  @Autowired
  public ProductsController(ProductsService productsService) {
    this.productsService = productsService;
  }

  @ResponseBody
  @GetMapping(value = "/autocomplete")
  public List<AutoCompletable> autocompleteProducts(Pageable pageable, @RequestParam(value = "q", required = false) String query) {
    List<ProductModel> products = productsService.getProducts(pageable, null, query, null, null, null).getContent();

    return AutocompleteService.autocomplete(products, query);
  }

  @GetMapping(value = "/sell-products")
  public String serveView() {
    return "sellproduct/index";
  }

  @GetMapping(params = {"barcode", "quantity", "fragmentName"})
  public String getProductByBarcode(@RequestParam Integer barcode, @RequestParam int quantity, @RequestParam String fragmentName, @RequestParam(required = false) Integer alreadyAdded, Model model) {
    if (alreadyAdded == null) alreadyAdded = 0;

    ProductModel productModel = productsService.getProductByBarcode(barcode, quantity, alreadyAdded);

    model.addAttribute("product", productModel);
    if (productModel.getImagePath() != null && !productModel.getImagePath().trim().isEmpty()) {
      model.addAttribute("picturesEndpoint", "http://" + medicineServiceUrl + "/products/picture/" + productModel.getImagePath());
    }

    return "sellproduct/fragments/" + fragmentName;
  }

  @PostMapping("/sell")
  public String sellProducts(@RequestBody List<ProductModel> productsList) {
    productsService.sellProducts(productsList);
    return "products/products_listview";
  }

  @PostMapping(value = "")
  public String createProduct(@ModelAttribute ProductModel productModel, @RequestParam("image") MultipartFile image) {
    ProductModel createdProduct = productsService.createProduct(productModel, image);

    return "redirect:/products";
  }

  @PostMapping(value = "/{id}")
  public String updateProduct(@PathVariable long id, @ModelAttribute ProductModel productModel, @RequestParam("image") MultipartFile image) {
    productsService.updateProduct(id, productModel, image);

    return "redirect:/products";
  }

  @DeleteMapping(value = "/{id}")
  public String deleteProduct(@PathVariable long id) {
    productsService.deleteProduct(id);

    return "redirect:/products";
  }

  @GetMapping(value = "/images")
  public ResponseEntity<Resource> getPicture(@RequestParam String name, HttpServletRequest request) {
    return productsService.getPicture(name, request);
  }

  @GetMapping(value = "")
  public String serveProductsOverview(Model model, Pageable pageable) {
    //TODO: Dummy data...
//    ProductModel productModel = new ProductModel();
//    productModel.setId(150L);
//    productModel.setBarcode(1241244142);
//    productModel.setBrandName("Picimilicin");
//    productModel.setQuantity(1);
//    productModel.setDrugId(1L);
//
//    TypeModel tm = new TypeModel();
//    tm.setName("Type Name");
//    productModel.setType(tm);
//
//    DrugModel dm = new DrugModel();
//    dm.setName("Licin");
//    productModel.setDrug(dm);
//
//    CountryModel cm = new CountryModel();
//    cm.setName("Kosova");
//    productModel.setProducerCountry(cm);
//
//    model.addAttribute("productsPage", new PageImpl<>(List.of(productModel), pageable,1));
//    model.addAttribute("totalPages", 1);

    Page<ProductModel> productsPage = productsService.getProducts(pageable, null, null, null, null, List.of("all"));

    model.addAttribute("productsPage", productsPage);

    return "products/products_listview";
  }

  @GetMapping(value = "/elements")
  public String serveProductsListElements(@RequestParam(required = false) Long productId, @RequestParam(required = false) String brandName, @RequestParam(required = false) Long typeId, @RequestParam(required = false) Long drugId, Model model, Pageable pageable) {
    Page<ProductModel> productsPage = productsService.getProducts(pageable, productId, brandName, typeId, drugId, List.of("all"));

    model.addAttribute("productsPage", productsPage);

    return "products/fragments/products_table";
  }

  @GetMapping(value = "/new")
  public String serveProductNew(Model model) {
    model.addAttribute("productModel", new ProductModel());

    model.addAttribute("postUrl", "/products");
    model.addAttribute("picturesEndpoint", "~/images/no-image.png");

    return "products/edit_product";
  }

  @GetMapping(value = "/view/{id}")
  public String serveProductView(Model model, @PathVariable long id) {
    ProductModel productModel = productsService.getProductById(id);

    convertEntersToNewLines(productModel);

    model.addAttribute("productModel", productModel);

    //Set external objects to fill dropdowns
    model.addAttribute("country", productModel.getProducerCountry().getName());
    model.addAttribute("type", productModel.getType().getName());
    model.addAttribute("drug", productModel.getDrug().getName());

    if (productModel.getImagePath() != null && !productModel.getImagePath().trim().isEmpty()) {
      model.addAttribute("picturesEndpoint", "http://" + medicineServiceUrl + "/products/picture/" + productModel.getImagePath());
    }

    return "products/view_product";
  }

  private ProductModel convertEntersToNewLines(ProductModel productModel) {
//    productModel.getDescription().replace;

    return productModel;
  }

  @GetMapping(value = "/{id}")
  public String serveProductEdit(Model model, @PathVariable long id) {
    ProductModel productModel = productsService.getProductById(id);

    model.addAttribute("productModel", productModel);

    //Set external objects to fill dropdowns
    model.addAttribute("countries", List.of(productModel.getProducerCountry()));
    model.addAttribute("types", List.of(productModel.getType()));
    model.addAttribute("drugs", List.of(productModel.getDrug()));

    if (productModel.getImagePath() != null && !productModel.getImagePath().trim().isEmpty()) {
      model.addAttribute("picturesEndpoint", "http://" + medicineServiceUrl + "/products/picture/" + productModel.getImagePath());
    }

    //Set the url to submit the form
    model.addAttribute("postUrl", "/products/" + id);
    return "products/edit_product";
  }
}
