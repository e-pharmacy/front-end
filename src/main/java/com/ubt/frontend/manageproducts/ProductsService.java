/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 02/08/2021.
 */

package com.ubt.frontend.manageproducts;

import com.ubt.frontend.manageproducts.models.PictureModel;
import com.ubt.frontend.manageproducts.models.ProductModel;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ProductsService {
  ProductModel getProductByBarcode(Integer barcode, int quantity, int alreadyAdded);

  Page<ProductModel> getProducts(Pageable pageable, Long productId, String brandName, Long typeId, Long drugId, List<String> expand);

  ProductModel getProductById(long id);

  ProductModel createProduct( ProductModel productModel, MultipartFile image);

  ProductModel updateProduct(long productId, ProductModel productModel, MultipartFile image);

  ProductModel deleteProduct(long productId);

  ResponseEntity<Resource> getPicture(String filename, HttpServletRequest request);

  PictureModel updatePicture(long productId, MultipartFile file);

  void deletePicture(long productId);

  void sellProducts(List<ProductModel> productsList);
}
