/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.frontend.manageproducts.models;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.models.BoBaseModel;
import com.ubt.frontend.managedrugs.models.DrugModel;
import com.ubt.frontend.staticdata.countries.models.CountryModel;
import com.ubt.frontend.staticdata.types.models.TypeModel;

public class ProductModel extends BoBaseModel implements AutoCompletable {
  private Integer barcode;
  private String imagePath;
  private Long drugId;
  private Long typeId;
  private Long producerCountryId;
  private String brandName;
  private String description;
  private String warning;
  private String prescription;
  private Boolean recommended;
  private Double pricePerUnit;
  private Double taxesPrice;
  private Double price;
  private Integer quantity;
  private Integer quantityLeftInStock;
  private Double subTotal;

  /**
   * Sub object fields
   */
  private CountryModel producerCountry;
  private TypeModel type;
  private DrugModel drug;

  public Double getSubTotal() {
    return subTotal;
  }

  public void setSubTotal(Double subTotal) {
    this.subTotal = subTotal;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public Double getTaxesPrice() {
    return taxesPrice;
  }

  public void setTaxesPrice(Double taxesPrice) {
    this.taxesPrice = taxesPrice;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Double getPricePerUnit() {
    return pricePerUnit;
  }

  public void setPricePerUnit(Double pricePerUnit) {
    this.pricePerUnit = pricePerUnit;
  }

  public Integer getBarcode() {
    return barcode;
  }

  public void setBarcode(Integer barcode) {
    this.barcode = barcode;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public Long getDrugId() {
    return drugId;
  }

  public void setDrugId(Long drugId) {
    this.drugId = drugId;
  }

  public Long getTypeId() {
    return typeId;
  }

  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }

  public Long getProducerCountryId() {
    return producerCountryId;
  }

  public void setProducerCountryId(Long producerCountryId) {
    this.producerCountryId = producerCountryId;
  }

  public String getBrandName() {
    return brandName;
  }

  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getWarning() {
    return warning;
  }

  public void setWarning(String warning) {
    this.warning = warning;
  }

  public String getPrescription() {
    return prescription;
  }

  public void setPrescription(String prescription) {
    this.prescription = prescription;
  }

  public Boolean getRecommended() {
    return recommended;
  }

  public void setRecommended(Boolean recommended) {
    this.recommended = recommended;
  }

  public CountryModel getProducerCountry() {
    return producerCountry;
  }

  public void setProducerCountry(CountryModel producerCountry) {
    this.producerCountry = producerCountry;
  }

  public TypeModel getType() {
    return type;
  }

  public void setType(TypeModel type) {
    this.type = type;
  }

  public DrugModel getDrug() {
    return drug;
  }

  public void setDrug(DrugModel drug) {
    this.drug = drug;
  }

  @Override
  public Object getAutocompleteId() {
    return id;
  }

  @Override
  public String getText() {
    return brandName;
  }

  public Integer getQuantityLeftInStock() {
    return quantityLeftInStock;
  }

  public void setQuantityLeftInStock(Integer quantityLeftInStock) {
    this.quantityLeftInStock = quantityLeftInStock;
  }
}
