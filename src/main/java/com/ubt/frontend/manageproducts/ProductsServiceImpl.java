/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 02/08/2021.
 */

package com.ubt.frontend.manageproducts;

import com.ubt.frontend.clients.MedicineFeignClient;
import com.ubt.frontend.clients.PosFeignClient;
import com.ubt.frontend.clients.ProductsPictureFeignClient;
import com.ubt.frontend.commons.exceptions.BadRequestException;
import com.ubt.frontend.commons.exceptions.NotFoundException;
import com.ubt.frontend.manageproducts.models.PictureModel;
import com.ubt.frontend.manageproducts.models.ProductModel;
import com.ubt.frontend.sales.builders.SaleModelBuilder;
import com.ubt.frontend.sales.models.SaleModel;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductsServiceImpl implements ProductsService {
  private final MedicineFeignClient medicineFeignClient;
  private final ProductsPictureFeignClient productsPictureFeignClient;
  private final PosFeignClient posFeignClient;
  private final int TAXES_PERCENTAGE = 10;

  @Autowired
  public ProductsServiceImpl(MedicineFeignClient medicineFeignClient, ProductsPictureFeignClient productsPictureFeignClient, PosFeignClient posFeignClient) {
    this.medicineFeignClient = medicineFeignClient;
    this.productsPictureFeignClient = productsPictureFeignClient;
    this.posFeignClient = posFeignClient;
  }

  @Override
  public ProductModel getProductByBarcode(Integer barcode, int quantity, int alreadyAdded) {
    //This is handled because when the service throws HTTP404, Feign thinks that endpoint is not found and interrupts the client request which results in 500HTTP.
    try {
      ProductModel productModel = medicineFeignClient.getProductByBarcode(barcode);

      if (productModel.getQuantityLeftInStock() < (quantity + alreadyAdded)) {
        throw new BadRequestException("Insufficient stock supplies! There are only " + (productModel.getQuantityLeftInStock() - alreadyAdded) + " product/s available in stock.");
      }

      renderProductModel(productModel, barcode, quantity);

      return productModel;
    } catch (FeignException e) {
      throw new NotFoundException("");
    }
  }

  private ProductModel renderProductModel(ProductModel productModel, Integer barcode, Integer quantity) {
    DecimalFormat df = new DecimalFormat("#.00");
    productModel.setPrice(productModel.getPricePerUnit() * quantity);
    productModel.setTaxesPrice(Double.valueOf(df.format((TAXES_PERCENTAGE * productModel.getPricePerUnit() / 100) * quantity)));
    productModel.setSubTotal(productModel.getPrice() + productModel.getTaxesPrice());
    productModel.setQuantity(quantity);

    return productModel;
  }

  @Override
  public Page<ProductModel> getProducts(Pageable pageable, Long productId, String brandName, Long typeId, Long drugId, List<String> expand) {
    return medicineFeignClient.getProducts(pageable, productId, brandName, typeId, drugId, expand);
  }

  @Override
  public ProductModel getProductById(long id) {
    return medicineFeignClient.getProductById(id);
  }

  @Override
  public ProductModel createProduct(ProductModel productModel, MultipartFile image) {
    ProductModel createdProduct = medicineFeignClient.createProduct(productModel);

    updatePicture(createdProduct.getId(), image);

    return createdProduct;
  }

  @Override
  public ProductModel updateProduct(long productId, ProductModel productModel, MultipartFile image) {
    ProductModel updatedProduct = medicineFeignClient.updateProduct(productId, productModel);

    updatePicture(productId, image);

    return updatedProduct;
  }

  @Override
  public ProductModel deleteProduct(long productId) {
    return medicineFeignClient.deleteProduct(productId);
  }

  @Override
  public ResponseEntity<Resource> getPicture(String filename, HttpServletRequest request) {
    return productsPictureFeignClient.getPicture(filename, request);
  }

  @Override
  public PictureModel updatePicture(long productId, MultipartFile file) {
    if (file.getOriginalFilename() != null && !file.getOriginalFilename().trim().isEmpty() && !file.getOriginalFilename().equals("no-image.png")) {
      return productsPictureFeignClient.updatePicture(productId, file);
    }

    return null;
  }

  @Override
  public void deletePicture(long productId) {
    productsPictureFeignClient.deletePicture(productId);
  }

  @Override
  public void sellProducts(List<ProductModel> productsList) {
    List<Integer> barcodes = productsList.stream().map(ProductModel::getBarcode).collect(Collectors.toList());
    Map<Integer, Integer> productsQuantityMap = productsList.stream().collect(Collectors.toMap(ProductModel::getBarcode, ProductModel::getQuantity));

    List<ProductModel> productsToSellList = medicineFeignClient.getProducts(null, null, barcodes, true);

    productsToSellList.forEach(e -> {
      int quantity = productsQuantityMap.get(e.getBarcode());
      e.setQuantity(quantity);
    });

    List<SaleModel> sales = convertProductsListToSaleModel(productsToSellList);

    //TODO: Branch POS location is set manually to 1, after implementation change this.
    sales.forEach(e -> e.setPharmacyLocationId(1L));

    posFeignClient.createSales(sales);
  }

  private List<SaleModel> convertProductsListToSaleModel(List<ProductModel> productsList) {
    List<SaleModel> salesList = new ArrayList<>();

    productsList.forEach(e -> {
      SaleModel saleModel = SaleModelBuilder.aSaleModel()
          .withProductId(e.getId())
          .withQuantity(e.getQuantity())
          .withSellPricePerUnit(e.getPricePerUnit())
          .build();

      salesList.add(saleModel);
    });

    return salesList;
  }
}
