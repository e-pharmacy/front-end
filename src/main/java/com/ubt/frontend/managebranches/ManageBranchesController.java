/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 23/09/2021.
 */

package com.ubt.frontend.managebranches;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.autocomplete.AutocompleteService;
import com.ubt.frontend.commons.models.BranchModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/branches")
public class ManageBranchesController {
  private final ManageBranchesService manageBranchesService;

  @Autowired
  public ManageBranchesController(ManageBranchesService manageBranchesService) {
    this.manageBranchesService = manageBranchesService;
  }

  @ResponseBody
  @GetMapping(value = "/autocomplete")
  public List<AutoCompletable> autocompleteBranches(@RequestParam(value = "q", required = false) String name) {
    List<BranchModel> branchModelList = manageBranchesService.getBranches(name);
    return AutocompleteService.autocomplete(branchModelList, name);
  }
}
