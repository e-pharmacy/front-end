/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 23/09/2021.
 */

package com.ubt.frontend.managebranches;

import com.ubt.frontend.commons.models.BranchModel;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ManageBranchesService {
  List<BranchModel> getBranches(String name);
}
