/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 23/09/2021.
 */

package com.ubt.frontend.managebranches;

import com.ubt.frontend.clients.StockFeignClient;
import com.ubt.frontend.commons.models.BranchModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManageBranchesServiceImpl implements ManageBranchesService {
  private final StockFeignClient stockFeignClient;

  @Autowired
  public ManageBranchesServiceImpl(StockFeignClient stockFeignClient) {
    this.stockFeignClient = stockFeignClient;
  }

  @Override
  public List<BranchModel> getBranches(String name) {
    return stockFeignClient.getBranches(null, name);
  }
}
