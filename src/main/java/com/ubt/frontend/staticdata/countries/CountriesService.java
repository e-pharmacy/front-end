package com.ubt.frontend.staticdata.countries;

import com.ubt.frontend.staticdata.countries.models.CountryModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CountriesService {
  Page<CountryModel> getCountries(String name, Pageable pageable);

  CountryModel getCountryById(long countryId);

  CountryModel createCountry(CountryModel countryModel);

  CountryModel updateCountry(long countryId, CountryModel countryModel);

  CountryModel deleteCountry(long countryId);
}
