package com.ubt.frontend.staticdata.countries;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.autocomplete.AutocompleteService;
import com.ubt.frontend.staticdata.countries.models.CountryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/countries")
public class CountriesController {
  private final CountriesService countriesService;

  @Autowired
  public CountriesController(CountriesService countriesService) {
    this.countriesService = countriesService;
  }

  @ResponseBody
  @GetMapping(value = "/autocomplete")
  public List<AutoCompletable> autocompleteCountries(Pageable pageable, @RequestParam(value = "q", required = false) String query) {
    List<CountryModel> countries = countriesService.getCountries(query, pageable).getContent();
    return AutocompleteService.autocomplete(countries, query);
  }
}
