package com.ubt.frontend.staticdata.countries;

import com.ubt.frontend.clients.MedicineFeignClient;
import com.ubt.frontend.staticdata.countries.models.CountryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CountriesServiceImpl implements CountriesService {
  private final MedicineFeignClient medicineFeignClient;

  @Autowired
  public CountriesServiceImpl(MedicineFeignClient medicineFeignClient) {
    this.medicineFeignClient = medicineFeignClient;
  }

  @Override
  public Page<CountryModel> getCountries(String name, Pageable pageable) {
    return medicineFeignClient.getCountries(pageable, name);
  }

  @Override
  public CountryModel getCountryById(long countryId) {
    return medicineFeignClient.getCountryById(countryId);
  }

  @Override
  public CountryModel createCountry(CountryModel countryModel) {
    return medicineFeignClient.createCountry(countryModel);
  }

  @Override
  public CountryModel updateCountry(long countryId, CountryModel countryModel) {
    return medicineFeignClient.updateCountry(countryId, countryModel);
  }

  @Override
  public CountryModel deleteCountry(long countryId) {
    return medicineFeignClient.deleteCountry(countryId);
  }
}
