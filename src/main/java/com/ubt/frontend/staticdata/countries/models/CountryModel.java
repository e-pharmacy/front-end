package com.ubt.frontend.staticdata.countries.models;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.models.BoBaseModel;

public class CountryModel extends BoBaseModel implements AutoCompletable {
  private String name;
  private String twoLettersName;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTwoLettersName() {
    return twoLettersName;
  }

  public void setTwoLettersName(String twoLettersName) {
    this.twoLettersName = twoLettersName;
  }

  @Override
  public Object getAutocompleteId() {
    return id;
  }

  @Override
  public String getText() {
    return name;
  }
}
