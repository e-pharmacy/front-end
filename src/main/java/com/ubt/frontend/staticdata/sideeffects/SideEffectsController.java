package com.ubt.frontend.staticdata.sideeffects;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.autocomplete.AutocompleteService;
import com.ubt.frontend.staticdata.sideeffects.models.SideEffectModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/side-effects")
public class SideEffectsController {
  private final SideEffectsService sideEffectsService;

  @Autowired
  public SideEffectsController(SideEffectsService sideEffectsService) {
    this.sideEffectsService = sideEffectsService;
  }

  @ResponseBody
  @GetMapping(value = "/autocomplete")
  public List<AutoCompletable> autocompleteSideEffects(Pageable pageable, @RequestParam(value = "q", required = false) String query) {
    List<SideEffectModel> sideEffects = sideEffectsService.getSideEffects(query, pageable).getContent();
    return AutocompleteService.autocomplete(sideEffects, query);
  }

  @GetMapping(value = "")
  public String serveSideEffectsOverview(Model model, Pageable pageable) {
    Page<SideEffectModel> sideEffectsPage = sideEffectsService.getSideEffects(null, pageable);

    model.addAttribute("sideEffectsPage", sideEffectsPage);

    return "sideeffects/side_effects_listview";
  }

  @GetMapping(value = "/elements")
  public String serveSideEffectsListElements(@RequestParam(required = false) String name, Model model, Pageable pageable) {
    Page<SideEffectModel> sideEffectsPage = sideEffectsService.getSideEffects(name, pageable);

    model.addAttribute("sideEffectsPage", sideEffectsPage);

    return "sideeffects/fragments/side_effects_table";
  }

  @GetMapping(value = "/new")
  public String serveSideEffectNew(Model model) {
    model.addAttribute("sideEffectModel", new SideEffectModel());

    model.addAttribute("postUrl", "/side-effects");
    return "sideeffects/edit_side_effect";
  }

  @GetMapping(value = "/{id}")
  public String serveSideEffectEdit(Model model, @PathVariable long id) {
    SideEffectModel sideEffectModel = sideEffectsService.getSideEffectById(id);

    model.addAttribute("sideEffectModel", sideEffectModel);

    //Set the url to submit the form
    model.addAttribute("postUrl", "/side-effects/" + id);
    return "sideeffects/edit_side_effect";
  }

  @PostMapping(value = "")
  public String createSideEffect(@ModelAttribute SideEffectModel sideEffectModel) {
    SideEffectModel createdSideEffect = sideEffectsService.createSideEffect(sideEffectModel);
    return "redirect:/side-effects";
  }

  @PostMapping(value = "/{id}")
  public String updateSideEffect(@PathVariable long id, @ModelAttribute SideEffectModel sideEffectModel) {
    sideEffectsService.updateSideEffect(id, sideEffectModel);
    return "redirect:/side-effects";
  }

  @DeleteMapping(value = "/{id}")
  public String deleteSideEffect(@PathVariable long id) {
    sideEffectsService.deleteSideEffect(id);

    return "redirect:/side-effects";
  }
}
