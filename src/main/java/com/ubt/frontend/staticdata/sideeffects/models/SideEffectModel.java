package com.ubt.frontend.staticdata.sideeffects.models;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.models.BoBaseModel;

public class SideEffectModel extends BoBaseModel implements AutoCompletable {
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  @Override
  public Object getAutocompleteId() {
    return id;
  }

  @Override
  public String getText() {
    return name;
  }
}
