package com.ubt.frontend.staticdata.sideeffects;

import com.ubt.frontend.clients.MedicineFeignClient;
import com.ubt.frontend.staticdata.sideeffects.models.SideEffectModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SideEffectsServiceImpl implements SideEffectsService {
  private final MedicineFeignClient medicineFeignClient;

  @Autowired
  public SideEffectsServiceImpl(MedicineFeignClient medicineFeignClient) {
    this.medicineFeignClient = medicineFeignClient;
  }

  @Override
  public Page<SideEffectModel> getSideEffects(String name, Pageable pageable) {
    return medicineFeignClient.getSideEffects(pageable, name);
  }

  @Override
  public Page<SideEffectModel> getSideEffectsByDrugsIds(List<Long> drugsIds, Pageable pageable) {
    return medicineFeignClient.getSideEffectsByDrugsIds(pageable, drugsIds);
  }

  @Override
  public SideEffectModel getSideEffectById(long sideEffectId) {
    return medicineFeignClient.getSideEffectById(sideEffectId);
  }

  @Override
  public SideEffectModel createSideEffect(SideEffectModel sideEffectModel) {
    return medicineFeignClient.createSideEffect(sideEffectModel);
  }

  @Override
  public SideEffectModel updateSideEffect(long sideEffectId, SideEffectModel sideEffectModel) {
    return medicineFeignClient.updateSideEffect(sideEffectId, sideEffectModel);
  }

  @Override
  public SideEffectModel deleteSideEffect(long sideEffectId) {
    return medicineFeignClient.deleteSideEffect(sideEffectId);
  }
}
