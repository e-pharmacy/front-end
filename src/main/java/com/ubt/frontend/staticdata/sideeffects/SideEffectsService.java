package com.ubt.frontend.staticdata.sideeffects;

import com.ubt.frontend.staticdata.sideeffects.models.SideEffectModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SideEffectsService {
  Page<SideEffectModel> getSideEffects(String name, Pageable pageable);

  Page<SideEffectModel> getSideEffectsByDrugsIds(List<Long> drugsIds, Pageable pageable);

  SideEffectModel getSideEffectById(long sideEffectId);

  SideEffectModel createSideEffect(SideEffectModel sideEffectModel);

  SideEffectModel updateSideEffect(long sideEffectId, SideEffectModel sideEffectModel);

  SideEffectModel deleteSideEffect(long sideEffectId);
}
