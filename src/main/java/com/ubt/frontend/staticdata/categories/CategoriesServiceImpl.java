package com.ubt.frontend.staticdata.categories;

import com.ubt.frontend.clients.MedicineFeignClient;
import com.ubt.frontend.staticdata.categories.models.CategoryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CategoriesServiceImpl implements CategoriesService {
  private final MedicineFeignClient medicineFeignClient;

  @Autowired
  public CategoriesServiceImpl(MedicineFeignClient medicineFeignClient) {
    this.medicineFeignClient = medicineFeignClient;
  }

  @Override
  public Page<CategoryModel> getCategories(String name, Pageable pageable) {
    return medicineFeignClient.getCategories(pageable, name);
  }

  @Override
  public CategoryModel getCategoryById(long categoryId) {
    return medicineFeignClient.getCategoryById(categoryId);
  }

  @Override
  public CategoryModel createCategory(CategoryModel categoryModel) {
    return medicineFeignClient.createCategory(categoryModel);
  }

  @Override
  public CategoryModel updateCategory(long categoryId, CategoryModel categoryModel) {
    return medicineFeignClient.updateCategory(categoryId, categoryModel);
  }

  @Override
  public CategoryModel deleteCategory(long categoryId) {
    return medicineFeignClient.deleteCategory(categoryId);
  }
}
