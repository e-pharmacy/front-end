package com.ubt.frontend.staticdata.categories;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.autocomplete.AutocompleteService;
import com.ubt.frontend.staticdata.categories.models.CategoryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/categories")
public class CategoriesController {
  private final CategoriesService categoriesService;

  @Autowired
  public CategoriesController(CategoriesService categoriesService) {
    this.categoriesService = categoriesService;
  }

  @ResponseBody
  @GetMapping(value = "/autocomplete")
  public List<AutoCompletable> autocompleteCategories(Pageable pageable, @RequestParam(value = "q", required = false) String query) {
    List<CategoryModel> categories = categoriesService.getCategories(query, pageable).getContent();
    return AutocompleteService.autocomplete(categories, query);
  }

  @GetMapping(value = "")
  public String serveCategoriesOverview(Model model, Pageable pageable) {
    Page<CategoryModel> categoriesPage = categoriesService.getCategories(null, pageable);

    model.addAttribute("categoriesPage", categoriesPage);

    return "categories/categories_listview";
  }

  @GetMapping(value = "/elements")
  public String serveCategoriesListElements(@RequestParam(required = false) String name, Model model, Pageable pageable) {
    Page<CategoryModel> categoriesPage = categoriesService.getCategories(name, pageable);

    model.addAttribute("categoriesPage", categoriesPage);

    return "categories/fragments/categories_table";
  }

  @GetMapping(value = "/new")
  public String serveCategoryNew(Model model) {
    model.addAttribute("categoryModel", new CategoryModel());

    model.addAttribute("postUrl", "/categories");
    return "categories/edit_category";
  }

  @GetMapping(value = "/{id}")
  public String serveCategoryEdit(Model model, @PathVariable long id) {
    CategoryModel categoryModel = categoriesService.getCategoryById(id);

    model.addAttribute("categoryModel", categoryModel);

    //Set the url to submit the form
    model.addAttribute("postUrl", "/categories/" + id);
    return "categories/edit_category";
  }

  @PostMapping(value = "")
  public String createCategory(@ModelAttribute CategoryModel categoryModel) {
    CategoryModel createdCategorie = categoriesService.createCategory(categoryModel);
    return "redirect:/categories";
  }

  @PostMapping(value = "/{id}")
  public String updateCategory(@PathVariable long id, @ModelAttribute CategoryModel categoryModel) {
    categoriesService.updateCategory(id, categoryModel);
    return "redirect:/categories";
  }

  @DeleteMapping(value = "/{id}")
  public String deleteCategory(@PathVariable long id) {
    categoriesService.deleteCategory(id);

    return "redirect:/categories";
  }
}
