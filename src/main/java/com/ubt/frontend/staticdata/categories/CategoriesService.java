package com.ubt.frontend.staticdata.categories;

import com.ubt.frontend.staticdata.categories.models.CategoryModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CategoriesService {
  Page<CategoryModel> getCategories(String name, Pageable pageable);

  CategoryModel getCategoryById(long categoryId);

  CategoryModel createCategory(CategoryModel categoryModel);

  CategoryModel updateCategory(long categoryId, CategoryModel categoryModel);

  CategoryModel deleteCategory(long categoryId);
}
