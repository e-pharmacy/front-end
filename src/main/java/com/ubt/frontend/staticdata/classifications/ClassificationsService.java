package com.ubt.frontend.staticdata.classifications;

import com.ubt.frontend.staticdata.classifications.models.ClassificationModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ClassificationsService {
  Page<ClassificationModel> getClassifications(String name, Pageable pageable);

  ClassificationModel getClassificationById(long classificationId);

  ClassificationModel createClassification(ClassificationModel classificationModel);

  ClassificationModel updateClassification(long classificationId, ClassificationModel classificationModel);

  ClassificationModel deleteClassification(long classificationId);
}
