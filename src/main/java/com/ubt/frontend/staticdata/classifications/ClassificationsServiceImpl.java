package com.ubt.frontend.staticdata.classifications;

import com.ubt.frontend.clients.MedicineFeignClient;
import com.ubt.frontend.staticdata.classifications.models.ClassificationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ClassificationsServiceImpl implements ClassificationsService {
  private final MedicineFeignClient medicineFeignClient;

  @Autowired
  public ClassificationsServiceImpl(MedicineFeignClient medicineFeignClient) {
    this.medicineFeignClient = medicineFeignClient;
  }

  @Override
  public Page<ClassificationModel> getClassifications(String name, Pageable pageable) {
    return medicineFeignClient.getClassifications(pageable, name);
  }

  @Override
  public ClassificationModel getClassificationById(long classificationId) {
    return medicineFeignClient.getClassificationById(classificationId);
  }

  @Override
  public ClassificationModel createClassification(ClassificationModel classificationModel) {
    return medicineFeignClient.createClassification(classificationModel);
  }

  @Override
  public ClassificationModel updateClassification(long classificationId, ClassificationModel classificationModel) {
    return medicineFeignClient.updateClassification(classificationId, classificationModel);
  }

  @Override
  public ClassificationModel deleteClassification(long classificationId) {
    return medicineFeignClient.deleteClassification(classificationId);
  }
}
