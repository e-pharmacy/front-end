package com.ubt.frontend.staticdata.classifications;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.autocomplete.AutocompleteService;
import com.ubt.frontend.staticdata.classifications.models.ClassificationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/classifications")
public class ClassificationsController {
  private final ClassificationsService classificationsService;

  @Autowired
  public ClassificationsController(ClassificationsService classificationsService) {
    this.classificationsService = classificationsService;
  }

  @ResponseBody
  @GetMapping(value = "/autocomplete")
  public List<AutoCompletable> autocompleteClassifications(Pageable pageable, @RequestParam(value = "q", required = false) String query) {
    List<ClassificationModel> classifications = classificationsService.getClassifications(query, pageable).getContent();
    return AutocompleteService.autocomplete(classifications, query);
  }

  @GetMapping(value = "")
  public String serveClassificationsOverview(Model model, Pageable pageable) {
    Page<ClassificationModel> classificationsPage = classificationsService.getClassifications(null, pageable);

    model.addAttribute("classificationsPage", classificationsPage);

    return "classifications/classifications_listview";
  }

  @GetMapping(value = "/elements")
  public String serveClassificationsListElements(@RequestParam(required = false) String name, Model model, Pageable pageable) {
    Page<ClassificationModel> classificationsPage = classificationsService.getClassifications(name, pageable);

    model.addAttribute("classificationsPage", classificationsPage);

    return "classifications/fragments/classifications_table";
  }

  @GetMapping(value = "/new")
  public String serveClassificationNew(Model model) {
    model.addAttribute("classificationModel", new ClassificationModel());

    model.addAttribute("postUrl", "/classifications");
    return "classifications/edit_classification";
  }

  @GetMapping(value = "/{id}")
  public String serveClassificationEdit(Model model, @PathVariable long id) {
    ClassificationModel classificationModel = classificationsService.getClassificationById(id);

    model.addAttribute("classificationModel", classificationModel);

    //Set the url to submit the form
    model.addAttribute("postUrl", "/classifications/" + id);
    return "classifications/edit_classification";
  }

  @PostMapping(value = "")
  public String createClassification(@ModelAttribute ClassificationModel classificationModel) {
    ClassificationModel createdClassification = classificationsService.createClassification(classificationModel);
    return "redirect:/classifications";
  }

  @PostMapping(value = "/{id}")
  public String updateClassification(@PathVariable long id, @ModelAttribute ClassificationModel classificationModel) {
    classificationsService.updateClassification(id, classificationModel);
    return "redirect:/classifications";
  }

  @DeleteMapping(value = "/{id}")
  public String deleteClassification(@PathVariable long id) {
    classificationsService.deleteClassification(id);

    return "redirect:/classifications";
  }
}
