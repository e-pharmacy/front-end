package com.ubt.frontend.staticdata.types;

import com.ubt.frontend.staticdata.types.models.TypeModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TypesService {
  Page<TypeModel> getTypes(String name, Pageable pageable);

  TypeModel getTypeById(long typeId);

  TypeModel createType(TypeModel typeModel);

  TypeModel updateType(long typeId, TypeModel typeModel);

  TypeModel deleteType(long typeId);
}
