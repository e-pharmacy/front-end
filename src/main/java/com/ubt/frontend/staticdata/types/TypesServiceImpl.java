package com.ubt.frontend.staticdata.types;

import com.ubt.frontend.clients.MedicineFeignClient;
import com.ubt.frontend.staticdata.types.models.TypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TypesServiceImpl implements TypesService {
  private final MedicineFeignClient medicineFeignClient;

  @Autowired
  public TypesServiceImpl(MedicineFeignClient medicineFeignClient) {
    this.medicineFeignClient = medicineFeignClient;
  }

  @Override
  public Page<TypeModel> getTypes(String name, Pageable pageable) {
    return medicineFeignClient.getTypes(pageable, name);
  }

  @Override
  public TypeModel getTypeById(long typeId) {
    return medicineFeignClient.getTypeById(typeId);
  }

  @Override
  public TypeModel createType(TypeModel typeModel) {
    return medicineFeignClient.createType(typeModel);
  }

  @Override
  public TypeModel updateType(long typeId, TypeModel typeModel) {
    return medicineFeignClient.updateType(typeId, typeModel);
  }

  @Override
  public TypeModel deleteType(long typeId) {
    return medicineFeignClient.deleteType(typeId);
  }
}
