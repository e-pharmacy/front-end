package com.ubt.frontend.staticdata.types.models;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.models.BoBaseModel;

public class TypeModel extends BoBaseModel implements AutoCompletable {
  private String name;
  private String description;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public Object getAutocompleteId() {
    return id;
  }

  @Override
  public String getText() {
    return name;
  }
}
