package com.ubt.frontend.staticdata.types;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.autocomplete.AutocompleteService;
import com.ubt.frontend.staticdata.types.models.TypeModel;
import com.ubt.frontend.staticdata.types.models.TypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/types")
public class TypesController {
  private final TypesService typesService;

  @Autowired
  public TypesController(TypesService typesService) {
    this.typesService = typesService;
  }

  @ResponseBody
  @GetMapping(value = "/autocomplete")
  public List<AutoCompletable> autocompleteTypes(Pageable pageable, @RequestParam(value = "q", required = false) String query) {
    List<TypeModel> types = typesService.getTypes(query, pageable).getContent();
    return AutocompleteService.autocomplete(types, query);
  }

  @GetMapping(value = "")
  public String serveTypesOverview(Model model, Pageable pageable) {
    Page<TypeModel> typesPage = typesService.getTypes(null, pageable);

    model.addAttribute("typesPage", typesPage);

    return "types/types_listview";
  }

  @GetMapping(value = "/elements")
  public String serveTypesListElements(@RequestParam(required = false) String name, Model model, Pageable pageable) {
    Page<TypeModel> typesPage = typesService.getTypes(name, pageable);

    model.addAttribute("typesPage", typesPage);

    return "types/fragments/types_table";
  }

  @GetMapping(value = "/new")
  public String serveTypeNew(Model model) {
    model.addAttribute("typeModel", new TypeModel());

    model.addAttribute("postUrl", "/types");
    return "types/edit_type";
  }

  @GetMapping(value = "/{id}")
  public String serveTypeEdit(Model model, @PathVariable long id) {
    TypeModel typeModel = typesService.getTypeById(id);

    model.addAttribute("typeModel", typeModel);

    //Set the url to submit the form
    model.addAttribute("postUrl", "/types/" + id);
    return "types/edit_type";
  }

  @PostMapping(value = "")
  public String createType(@ModelAttribute TypeModel typeModel) {
    TypeModel createdType = typesService.createType(typeModel);
    return "redirect:/types";
  }

  @PostMapping(value = "/{id}")
  public String updateType(@PathVariable long id, @ModelAttribute TypeModel typeModel) {
    typesService.updateType(id, typeModel);
    return "redirect:/types";
  }

  @DeleteMapping(value = "/{id}")
  public String deleteType(@PathVariable long id) {
    typesService.deleteType(id);

    return "redirect:/types";
  }
}
