package com.ubt.frontend.config.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JacksonConfig {
  private final ObjectMapper objectMapper;


  @Autowired
  public JacksonConfig(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
    configObjectMapper(this.objectMapper);
  }

  private void configObjectMapper(ObjectMapper objectMapper) {
    objectMapper.registerModule(new PageModule());
  }
}
