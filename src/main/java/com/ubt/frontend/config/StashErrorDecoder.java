/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 01/10/2021.
 */

package com.ubt.frontend.config;

import com.ubt.frontend.commons.exceptions.NotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

import static feign.FeignException.errorStatus;

public class StashErrorDecoder implements ErrorDecoder {

  @Override
  public Exception decode(String methodKey, Response response) {
    // This is handled because Feign thinks endpoint is not found and interrupts the client request and throws 500HTTP automatically.
    if (response.status() == 404) {
      throw new NotFoundException("");
    }

    return errorStatus(methodKey, response);
  }
}
