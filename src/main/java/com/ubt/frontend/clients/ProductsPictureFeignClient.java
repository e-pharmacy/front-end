package com.ubt.frontend.clients;

import com.ubt.frontend.manageproducts.models.PictureModel;
import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@Service
@FeignClient(value = "products-pictures", url = "${feign.address.medicine-service}", configuration = ProductsPictureFeignClient.FeignConfiguration.class)
public interface ProductsPictureFeignClient {
  @GetMapping("/products/picture/{filename:.+}")
  ResponseEntity<Resource> getPicture(@PathVariable String filename, @SpringQueryMap HttpServletRequest request);

  @PostMapping(value = "/products/{productId}/picture", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  PictureModel updatePicture(@PathVariable long productId, @RequestPart("file") MultipartFile file);

  @DeleteMapping("/products/{productId}/picture")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  void deletePicture(@PathVariable long productId);

  class FeignConfiguration {
    @Bean
    public Encoder multipartFormEncoder() {
      return new SpringFormEncoder(new SpringEncoder(() -> new HttpMessageConverters(new RestTemplate().getMessageConverters())));
    }
  }
}
