package com.ubt.frontend.clients;

import com.ubt.frontend.alertsconfiguration.models.AlertConditionModel;
import com.ubt.frontend.commons.models.AlertConditionTypeModel;
import com.ubt.frontend.commons.models.BranchModel;
import com.ubt.frontend.manageproducts.models.SupplyModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
@FeignClient(value = "stock-service", url = "${feign.address.stock-service}")
public interface StockFeignClient {
  @GetMapping(value = "/alerts/conditions/types")
  List<AlertConditionTypeModel> getAlertTypes(@RequestParam(required = false) String name);

  @GetMapping(value = "/alerts/conditions")
  List<AlertConditionModel> getAlertConditions(@RequestParam(required = false) List<Long> alertConditionsIds, @RequestParam(required = false) Long branchId,
                                               @RequestParam(required = false) Long productId, @RequestParam(required = false) Long typeId);

  @PostMapping("/alerts/conditions")
  AlertConditionModel createAlertCondition(@RequestBody AlertConditionModel alertConditionModel);

  @DeleteMapping("/alerts/conditions/{id}")
  AlertConditionModel deleteAlertCondition(@PathVariable Long id);

  @GetMapping("/supplies")
  Page<SupplyModel> getSupplies(@RequestParam(required = false) List<Long> ids, @RequestParam(required = false) Long productId, @RequestParam(required = false) Long branchId, Pageable pageable);

  @GetMapping("/branches")
  List<BranchModel> getBranches(@RequestParam(required = false) List<Long> ids, @RequestParam(required = false) String name);

  @PostMapping("/supplies")
  SupplyModel createSupply(@RequestBody SupplyModel supplyModel);

  @DeleteMapping("/supplies/{id}")
  SupplyModel deleteSupply(@PathVariable Long id);
}
