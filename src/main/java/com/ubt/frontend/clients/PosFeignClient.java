package com.ubt.frontend.clients;

import com.ubt.frontend.manageproducts.models.ProductModel;
import com.ubt.frontend.sales.models.MonthlySaleModel;
import com.ubt.frontend.sales.models.MonthlySaleProfitModel;
import com.ubt.frontend.sales.models.SaleModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
@FeignClient(value = "pos-service", url = "${feign.address.pos-service}")
public interface PosFeignClient {
  @GetMapping(value = "/sales/reports", params = {"reports=TopProductSales"})
  List<SaleModel> getTop5ProductSalesFromLastMonth(@RequestParam String reports);

  @GetMapping(value = "/sales/reports")
  List<SaleModel> getBranchSalesFromLastMonth(@RequestParam String reports);

  @GetMapping(value = "/sales/reports")
  List<MonthlySaleModel> getMonthlySalesFromLastYear(@RequestParam String reports);

  @GetMapping(value = "/sales/reports")
  List<MonthlySaleProfitModel> getMonthlySalesProfitFromLastYear(@RequestParam String reports);

  @PostMapping(value = "/sales")
  List<SaleModel> createSales(@RequestBody List<SaleModel> saleModel);
}
