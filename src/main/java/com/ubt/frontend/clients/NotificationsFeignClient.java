package com.ubt.frontend.clients;

import com.ubt.frontend.notifications.models.NotificationModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Service
@FeignClient(value = "notifications-service", url = "${feign.address.notifications-service}")
public interface NotificationsFeignClient {
  @GetMapping("/users/{userId}")
  Page<NotificationModel> getNotificationByUserId(Pageable pageable, @PathVariable long userId);
}
