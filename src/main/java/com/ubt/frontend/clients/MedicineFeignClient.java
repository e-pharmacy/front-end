package com.ubt.frontend.clients;

import com.ubt.frontend.managedrugs.models.DrugModel;
import com.ubt.frontend.manageproducts.models.ProductModel;
import com.ubt.frontend.staticdata.categories.models.CategoryModel;
import com.ubt.frontend.staticdata.classifications.models.ClassificationModel;
import com.ubt.frontend.staticdata.countries.models.CountryModel;
import com.ubt.frontend.staticdata.sideeffects.models.SideEffectModel;
import com.ubt.frontend.staticdata.types.models.TypeModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
@FeignClient(value = "medicine-service", url = "${feign.address.medicine-service}")
public interface MedicineFeignClient {
  //===============================================================================
  // Products
  //===============================================================================
  @GetMapping(value = "/products")
  ProductModel getProductByBarcode(@RequestParam Integer barcode);

  @GetMapping("/products")
  Page<ProductModel> getProducts(Pageable pageable, @RequestParam(required = false) Long productId, @RequestParam(required = false) String brandName, @RequestParam(required = false) Long typesIds, @RequestParam(required = false) Long drugsIds, @RequestParam(required = false) List<String> expand);

  /**
   * This endpoint uses FilterModel, I have added only the filter fields that was needed for my calls. Feel free to add as much as there are on ProductFilter object supported.
   *
   * @param brandName
   * @param unPaged
   * @return
   */
  @GetMapping("/products")
  List<ProductModel> getProducts(@RequestParam(required = false) Long productId, @RequestParam(required = false) String brandName, @RequestParam(required = false) List<Integer> barcodes, @RequestParam boolean unPaged);

  @GetMapping("/products/{productId}")
  ProductModel getProductById(@PathVariable long productId);

  @PostMapping("/products")
  ProductModel createProduct(@RequestBody ProductModel productModel);

  @PutMapping("/products/{productId}")
  ProductModel updateProduct(@PathVariable long productId, @RequestBody ProductModel productModel);

  @DeleteMapping(value = "/products/{productId}")
  ProductModel deleteProduct(@PathVariable long productId);

  //===============================================================================
  // Drugs
  //===============================================================================
  @GetMapping("/drugs")
  Page<DrugModel> getDrugs(Pageable pageable, @RequestParam(required = false) String name, @RequestParam(required = false) Long classificationsIds, @RequestParam(required = false) Long categoriesIds, @RequestParam(required = false) List<String> expand);

  @GetMapping("/drugs/{drugId}")
  DrugModel getDrugById(@PathVariable long drugId);

  @PostMapping("/drugs")
  DrugModel createDrug(@RequestBody DrugModel drugModel);

  @PutMapping("/drugs/{drugId}")
  DrugModel updateDrug(@PathVariable long drugId, @RequestBody DrugModel drugModel);

  @DeleteMapping("/drugs/{drugId}")
  DrugModel deleteDrug(@PathVariable long drugId);

  @PostMapping("/drugs/{drugId}/side-effects")
  void addDrugSideEffects(@PathVariable long drugId, @RequestParam List<Long> sideEffectIds);

  //===============================================================================
  // Categories
  //===============================================================================
  @GetMapping("/categories")
  Page<CategoryModel> getCategories(Pageable pageable, @RequestParam("name") String name);

  @GetMapping("/categories/{categoryId}")
  CategoryModel getCategoryById(@PathVariable long categoryId);

  @PostMapping("/categories")
  CategoryModel createCategory(@RequestBody CategoryModel categoryModel);

  @PutMapping("/categories/{categoryId}")
  CategoryModel updateCategory(@PathVariable long categoryId, @RequestBody CategoryModel categoryModel);

  @DeleteMapping("/categories/{categoryId}")
  CategoryModel deleteCategory(@PathVariable long categoryId);

  //===============================================================================
  // Classifications
  //===============================================================================
  @GetMapping("/classifications")
  Page<ClassificationModel> getClassifications(Pageable pageable, @RequestParam("name") String name);

  @GetMapping("/classifications/{classificationId}")
  ClassificationModel getClassificationById(@PathVariable long classificationId);

  @PostMapping("/classifications")
  ClassificationModel createClassification(@RequestBody ClassificationModel classificationModel);

  @PutMapping("/classifications/{classificationId}")
  ClassificationModel updateClassification(@PathVariable long classificationId, @RequestBody ClassificationModel classificationModel);

  @DeleteMapping("/classifications/{classificationId}")
  ClassificationModel deleteClassification(@PathVariable long classificationId);

  //===============================================================================
  // Countries
  //===============================================================================
  @GetMapping("/countries")
  Page<CountryModel> getCountries(Pageable pageable, @RequestParam("name") String name);

  @GetMapping("/countries/{countryId}")
  CountryModel getCountryById(@PathVariable long countryId);

  @PostMapping("/countries")
  CountryModel createCountry(@RequestBody CountryModel countryModel);

  @PutMapping("/countries/{countryId}")
  CountryModel updateCountry(@PathVariable long countryId, @RequestBody CountryModel countryModel);

  @DeleteMapping("/countries/{countryId}")
  CountryModel deleteCountry(@PathVariable long countryId);

  //===============================================================================
  // Side Effects
  //===============================================================================
  @GetMapping("/side-effects")
  Page<SideEffectModel> getSideEffects(Pageable pageable, @RequestParam("name") String name);

  @GetMapping("/side-effects")
  Page<SideEffectModel> getSideEffectsByDrugsIds(Pageable pageable, @RequestParam("drugsIds") List<Long> drugsIds);

  @GetMapping("/side-effects/{sideEffectId}")
  SideEffectModel getSideEffectById(@PathVariable long sideEffectId);

  @PostMapping("/side-effects")
  SideEffectModel createSideEffect(@RequestBody SideEffectModel sideEffectModel);

  @PutMapping("/side-effects/{sideEffectId}")
  SideEffectModel updateSideEffect(@PathVariable long sideEffectId, @RequestBody SideEffectModel sideEffectModel);

  @DeleteMapping("/side-effects/{sideEffectId}")
  SideEffectModel deleteSideEffect(@PathVariable long sideEffectId);

  //===============================================================================
  // Types
  //===============================================================================
  @GetMapping("/types")
  Page<TypeModel> getTypes(Pageable pageable, @RequestParam("name") String name);

  @GetMapping("/types/{typeId}")
  TypeModel getTypeById(@PathVariable long typeId);

  @PostMapping("/types")
  TypeModel createType(@RequestBody TypeModel typeModel);

  @PutMapping("/types/{typeId}")
  TypeModel updateType(@PathVariable long typeId, @RequestBody TypeModel typeModel);

  @DeleteMapping("/types/{typeId}")
  TypeModel deleteType(@PathVariable long typeId);
}
