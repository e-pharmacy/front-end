/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 13/09/2021.
 */

package com.ubt.frontend.sales;

import com.ubt.frontend.manageproducts.models.ProductModel;
import com.ubt.frontend.manageproducts.models.SupplyModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/sales")
public class SalesController {
  private final SalesService salesService;

  @Autowired
  public SalesController(SalesService salesService) {
    this.salesService = salesService;
  }

  @GetMapping(value = "")
  public String serveOverview(Model model) {
    model.addAttribute("productSalesList", salesService.getTop5ProductSalesFromLastMonth());
    model.addAttribute("branchSalesList", salesService.getBranchSalesFromLastMonth());
    model.addAttribute("monthlySalesGraph", salesService.getMonthlySalesFromLastYear());
    model.addAttribute("overviewChart", salesService.getMonthlySalesProfitFromLastYear());

    return "sales/index";
  }
}
