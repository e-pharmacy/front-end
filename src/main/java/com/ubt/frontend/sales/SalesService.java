/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 13/09/2021.
 */

package com.ubt.frontend.sales;

import com.ubt.frontend.manageproducts.models.ProductModel;
import com.ubt.frontend.manageproducts.models.SupplyModel;
import com.ubt.frontend.sales.models.MonthlySaleModel;
import com.ubt.frontend.sales.models.MonthlySaleProfitModel;
import com.ubt.frontend.sales.models.SaleModel;

import java.util.List;

public interface SalesService {
  List<SaleModel> getBranchSalesFromLastMonth();

  List<SaleModel> getTop5ProductSalesFromLastMonth();

  List<MonthlySaleModel> getMonthlySalesFromLastYear();

  List<MonthlySaleProfitModel> getMonthlySalesProfitFromLastYear();
}
