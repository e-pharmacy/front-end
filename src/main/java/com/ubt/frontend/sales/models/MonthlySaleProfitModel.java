/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 21/09/2021.
 */

package com.ubt.frontend.sales.models;

import com.ubt.frontend.commons.models.BoBaseModel;

public class MonthlySaleProfitModel extends BoBaseModel {
  public Double profit;
  private String month;

  public Double getProfit() {
    return profit;
  }

  public void setProfit(Double profit) {
    this.profit = profit;
  }

  public String getMonth() {
    return month;
  }

  public void setMonth(String month) {
    this.month = month;
  }
}
