/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 13/09/2021.
 */

package com.ubt.frontend.sales.models;

import java.util.List;

public class OverviewChartModel {
  private List<String> months;
  private List<Integer> data;

  public List<String> getMonths() {
    return months;
  }

  public void setMonths(List<String> months) {
    this.months = months;
  }

  public List<Integer> getData() {
    return data;
  }

  public void setData(List<Integer> data) {
    this.data = data;
  }
}
