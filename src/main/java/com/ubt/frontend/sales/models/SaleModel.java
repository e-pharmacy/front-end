/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.frontend.sales.models;

import com.ubt.frontend.commons.models.BoBaseModel;
import com.ubt.frontend.commons.models.BranchModel;
import com.ubt.frontend.manageproducts.models.ProductModel;

public class SaleModel extends BoBaseModel {
  private Integer quantity;
  private Long pharmacyLocationId;
  private BranchModel branchModel;
  private ProductModel productModel;
  private Long productId;
  private Integer salesCount;
  private Double sellPricePerUnit;

  public Double getSellPricePerUnit() {
    return sellPricePerUnit;
  }

  public void setSellPricePerUnit(Double sellPricePerUnit) {
    this.sellPricePerUnit = sellPricePerUnit;
  }

  public BranchModel getBranchModel() {
    return branchModel;
  }

  public void setBranchModel(BranchModel branchModel) {
    this.branchModel = branchModel;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public Integer getSalesCount() {
    return salesCount;
  }

  public void setSalesCount(Integer salesCount) {
    this.salesCount = salesCount;
  }

  public ProductModel getProductModel() {
    return productModel;
  }

  public void setProductModel(ProductModel productModel) {
    this.productModel = productModel;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public Long getPharmacyLocationId() {
    return pharmacyLocationId;
  }

  public void setPharmacyLocationId(Long pharmacyLocationId) {
    this.pharmacyLocationId = pharmacyLocationId;
  }
}
