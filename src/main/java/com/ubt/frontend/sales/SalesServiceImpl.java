/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 13/09/2021.
 */

package com.ubt.frontend.sales;

import com.ubt.frontend.clients.PosFeignClient;
import com.ubt.frontend.sales.models.MonthlySaleModel;
import com.ubt.frontend.sales.models.MonthlySaleProfitModel;
import com.ubt.frontend.sales.models.SaleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalesServiceImpl implements SalesService {
  private final PosFeignClient posFeignClient;

  @Autowired
  public SalesServiceImpl(PosFeignClient posFeignClient) {
    this.posFeignClient = posFeignClient;
  }

  @Override
  public List<SaleModel> getBranchSalesFromLastMonth() {
    List<SaleModel> branchSalesList = posFeignClient.getBranchSalesFromLastMonth("branchSales");
    branchSalesList.sort((o1, o2) -> {
      Long branchModelId1 = o1.getBranchModel().getId();
      Long branchModelId2 = o2.getBranchModel().getId();

      if (branchModelId1 > branchModelId2) {
        return 1;
      } else if (branchModelId1 < branchModelId2) {
        return -1;
      }
      return 0;
    });

    return branchSalesList;
  }

  @Override
  public List<SaleModel> getTop5ProductSalesFromLastMonth() {
    return posFeignClient.getTop5ProductSalesFromLastMonth("TopProductSales");
  }

  @Override
  public List<MonthlySaleModel> getMonthlySalesFromLastYear() {
    return posFeignClient.getMonthlySalesFromLastYear("monthlySales");
  }

  @Override
  public List<MonthlySaleProfitModel> getMonthlySalesProfitFromLastYear() {
    return posFeignClient.getMonthlySalesProfitFromLastYear("monthlySalesProfit");
  }
}
