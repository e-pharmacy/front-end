/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 22/09/2021.
 */

package com.ubt.frontend.managesupplies;

import com.ubt.frontend.manageproducts.models.SupplyModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ManageSuppliesService {
  Page<SupplyModel> getSupplies(Long productId, Long branchId, Pageable pageable);

  SupplyModel createSupply(SupplyModel supplyModel);

  void deleteSupply(long id);
}
