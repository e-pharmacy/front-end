/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 22/09/2021.
 */

package com.ubt.frontend.managesupplies;

import com.ubt.frontend.clients.MedicineFeignClient;
import com.ubt.frontend.clients.StockFeignClient;
import com.ubt.frontend.manageproducts.models.ProductModel;
import com.ubt.frontend.manageproducts.models.SupplyModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManageSuppliesServiceImpl implements ManageSuppliesService {
  private final StockFeignClient stockFeignClient;
  private final MedicineFeignClient medicineFeignClient;

  @Autowired
  public ManageSuppliesServiceImpl(StockFeignClient stockFeignClient, MedicineFeignClient medicineFeignClient) {
    this.stockFeignClient = stockFeignClient;
    this.medicineFeignClient = medicineFeignClient;
  }

  @Override
  public Page<SupplyModel> getSupplies(Long productId, Long branchId, Pageable pageable) {
    return stockFeignClient.getSupplies(null, productId, branchId, pageable);
  }

  @Override
  public SupplyModel createSupply(SupplyModel supplyModel) {
    return stockFeignClient.createSupply(supplyModel);
  }

  @Override
  public void deleteSupply(long id) {
    stockFeignClient.deleteSupply(id);
  }
}
