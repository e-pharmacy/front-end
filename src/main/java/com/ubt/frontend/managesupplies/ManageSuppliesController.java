/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 22/09/2021.
 */

package com.ubt.frontend.managesupplies;

import com.ubt.frontend.commons.DateTimeService;
import com.ubt.frontend.manageproducts.models.SupplyModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/supplies")
public class ManageSuppliesController {
  private final ManageSuppliesService manageSuppliesService;

  @Autowired
  public ManageSuppliesController(ManageSuppliesService manageSuppliesService) {
    this.manageSuppliesService = manageSuppliesService;
  }

  @GetMapping(value = "")
  public String serveView(Model model, Pageable pageable) {
    model.addAttribute("supplies", manageSuppliesService.getSupplies(null, null, pageable));

    return "supplies/index";
  }

  @GetMapping(value = "/elements")
  public String serveProductsListElements(@RequestParam(required = false) Long productId, @RequestParam(required = false) Long branchId, Model model, Pageable pageable) {
    model.addAttribute("supplies", manageSuppliesService.getSupplies(productId, branchId, pageable));

    return "supplies/fragments/supplies_table";
  }

  @GetMapping(value = "/new")
  public String serveSupplyNew(Model model) {
    model.addAttribute("supplyModel", new SupplyModel());
    return "supplies/create_supply";
  }

  @PostMapping(value = "")
  public String createSupply(@ModelAttribute SupplyModel supplyModel, @RequestParam String expirationDateString) {
    Long expirationDate = DateTimeService.convertStringDateToTimeStamp(expirationDateString);
    supplyModel.setExpirationDate(expirationDate);
    
    manageSuppliesService.createSupply(supplyModel);
    return "redirect:/supplies";
  }

  @DeleteMapping(value = "/{id}")
  public String deleteSupply(@PathVariable long id) {
    manageSuppliesService.deleteSupply(id);

    return "redirect:/supplies";
  }
}
