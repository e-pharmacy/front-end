/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 24/09/2021.
 */

package com.ubt.frontend.commons;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateTimeService {
  public static Long convertStringDateToTimeStamp(String date) {
    DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    try {
      Date dateObject = df.parse(date + " 00:00:00");
      return dateObject.getTime();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }
}
