package com.ubt.frontend.commons.models;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;

public class BranchModel extends BoBaseModel implements AutoCompletable {
  private String address;

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  @Override
  public Object getAutocompleteId() {
    return id;
  }

  @Override
  public String getText() {
    return address;
  }
}
