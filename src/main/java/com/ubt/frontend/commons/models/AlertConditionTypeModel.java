package com.ubt.frontend.commons.models;

import com.ubt.frontend.commons.autocomplete.AutoCompletable;

public class AlertConditionTypeModel extends BoBaseModel implements AutoCompletable {
  private String description;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  @Override
  public Object getAutocompleteId() {
    return id;
  }

  @Override
  public String getText() {
    return description;
  }
}
