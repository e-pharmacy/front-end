package com.ubt.frontend.commons.autocomplete;

import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

public class AutocompleteService {
  public static final int RESULT_SIZE = 15;

  public static List<AutoCompletable> autocomplete(List<? extends AutoCompletable> content, String query) {
    if (StringUtils.isEmpty(query)) {
      return content.stream()
              .limit(RESULT_SIZE)
              .collect(Collectors.toList());
    }

    return content.stream()
            .filter(e -> e.getText()
                    .toLowerCase()
                    .contains(query))
            .limit(RESULT_SIZE)
            .collect(Collectors.toList());
  }
}
