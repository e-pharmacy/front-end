package com.ubt.frontend.commons.autocomplete;

/**
 * Common Interface for models that should have autocomplete functionality
 */
public interface AutoCompletable {
  Object getAutocompleteId();
  String getText();
}
