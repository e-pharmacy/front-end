package com.ubt.frontend.commons.notifications;

import com.ubt.frontend.notifications.models.NotificationModel;

import java.util.List;

public class NotificationsDataResolver {
  private static final Long TEST_TYPE = 1L;

  public static void resolveData(List<NotificationModel> notifications) {
    for (NotificationModel notification: notifications) {
      Long typeId = notification.getTypeId();

      if (TEST_TYPE.equals(typeId)) {
        notification.setText("Drug Alerts");
        notification.setBody("Paracetamol down to 5 in stock");
      }
      notification.setTime("2 hours ago");
    }
  }
}
