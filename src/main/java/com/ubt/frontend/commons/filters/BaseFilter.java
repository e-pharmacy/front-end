package com.ubt.frontend.commons.filters;

import java.util.List;

public abstract class BaseFilter {
  private List<Long> ids;

  public List<Long> getIds() {
    return ids;
  }

  public void setIds(List<Long> ids) {
    this.ids = ids;
  }
}
