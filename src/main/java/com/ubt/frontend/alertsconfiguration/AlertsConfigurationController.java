/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.frontend.alertsconfiguration;

import com.ubt.frontend.alertsconfiguration.models.AlertConditionModel;
import com.ubt.frontend.commons.autocomplete.AutoCompletable;
import com.ubt.frontend.commons.autocomplete.AutocompleteService;
import com.ubt.frontend.commons.models.AlertConditionTypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/alerts")
public class AlertsConfigurationController {
  private final AlertsConfigurationService alertsConfigurationService;

  @Autowired
  public AlertsConfigurationController(AlertsConfigurationService alertsConfigurationService) {
    this.alertsConfigurationService = alertsConfigurationService;
  }

  @ResponseBody
  @GetMapping(value = "/types/autocomplete")
  public List<AutoCompletable> autocompleteAlertTypes(@RequestParam(value = "q", required = false) String query) {
    List<AlertConditionTypeModel> alertTypes = alertsConfigurationService.getAlertTypes(query);

    return AutocompleteService.autocomplete(alertTypes, query);
  }

  @GetMapping("")
  public String serveOverview(Model model) {
    List<AlertConditionModel> alertConditionModelList = alertsConfigurationService.getAlerts(null, null, null);
    model.addAttribute("alertsConfigurations", alertConditionModelList);

    return "alertsconfiguration/alerts_listview";
  }

  @GetMapping("/elements")
  public String serveAlertsListElements(Model model, @RequestParam(required = false) Long productId, @RequestParam(required = false) Long alertTypeId, @RequestParam(required = false) Long branchId) {
    List<AlertConditionModel> alertConditionModelList = alertsConfigurationService.getAlerts(productId, alertTypeId, branchId);
    model.addAttribute("alertsConfigurations", alertConditionModelList);

    return "alertsconfiguration/fragments/alerts_table";
  }

  @GetMapping("/new")
  public String serveAlertsNew(Model model) {
    model.addAttribute("alertConditionModel", new AlertConditionModel());

    return "alertsconfiguration/new_alert";
  }

  @PostMapping("")
  public String createAlertCondition(@ModelAttribute AlertConditionModel alertConditionModel) {
    alertsConfigurationService.createAlertCondition(alertConditionModel);

    return "redirect:/alerts";
  }

  @DeleteMapping("/{id}")
  public String deleteAlertCondition(@PathVariable Long id) {
    alertsConfigurationService.deleteAlertCondition(id);

    return "redirect:/alerts";
  }
}
