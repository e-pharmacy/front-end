/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 02/08/2021.
 */

package com.ubt.frontend.alertsconfiguration;

import com.ubt.frontend.alertsconfiguration.models.AlertConditionModel;
import com.ubt.frontend.clients.StockFeignClient;
import com.ubt.frontend.commons.models.AlertConditionTypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlertsConfigurationServiceImpl implements AlertsConfigurationService {
  private final StockFeignClient stockFeignClient;

  @Autowired
  public AlertsConfigurationServiceImpl(StockFeignClient stockFeignClient) {
    this.stockFeignClient = stockFeignClient;
  }

  @Override
  public List<AlertConditionTypeModel> getAlertTypes(String name) {
    return stockFeignClient.getAlertTypes(name);
  }

  @Override
  public List<AlertConditionModel> getAlerts(Long productId, Long alertTypeId, Long branchId) {
    return stockFeignClient.getAlertConditions(null, branchId, productId, alertTypeId);
  }

  @Override
  public AlertConditionModel deleteAlertCondition(Long id) {
    return stockFeignClient.deleteAlertCondition(id);
  }

  @Override
  public AlertConditionModel createAlertCondition(AlertConditionModel alertConditionModel) {
    return stockFeignClient.createAlertCondition(alertConditionModel);
  }
}
