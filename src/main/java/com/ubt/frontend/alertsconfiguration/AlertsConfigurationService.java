/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 02/08/2021.
 */

package com.ubt.frontend.alertsconfiguration;

import com.ubt.frontend.alertsconfiguration.models.AlertConditionModel;
import com.ubt.frontend.commons.models.AlertConditionTypeModel;

import java.util.List;

public interface AlertsConfigurationService {
  List<AlertConditionTypeModel> getAlertTypes(String name);

  List<AlertConditionModel> getAlerts(Long productId, Long alertTypeId, Long branchId);

  AlertConditionModel deleteAlertCondition(Long id);

  AlertConditionModel createAlertCondition(AlertConditionModel alertConditionModel);
}
