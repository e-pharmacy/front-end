/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 06/08/2021.
 */

package com.ubt.frontend.alertsconfiguration.builders;

import com.ubt.frontend.alertsconfiguration.models.AlertConditionModel;
import com.ubt.frontend.commons.models.AlertConditionTypeModel;
import com.ubt.frontend.commons.models.BranchModel;
import com.ubt.frontend.manageproducts.models.ProductModel;

public final class AlertConditionModelBuilder {
  private Long alertConditionTypeId;
  private Long pharmacyLocationId;
  private Long productId;
  private Integer quantity;
  private BranchModel branchModel;
  private AlertConditionTypeModel alertConditionType;
  private ProductModel productModel;
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long createdBy;
  private Long updatedBy;

  private AlertConditionModelBuilder() {
  }

  public static AlertConditionModelBuilder anAlertConditionModel() {
    return new AlertConditionModelBuilder();
  }

  public AlertConditionModelBuilder withAlertConditionTypeId(Long alertConditionTypeId) {
    this.alertConditionTypeId = alertConditionTypeId;
    return this;
  }

  public AlertConditionModelBuilder withPharmacyLocationId(Long pharmacyLocationId) {
    this.pharmacyLocationId = pharmacyLocationId;
    return this;
  }

  public AlertConditionModelBuilder withProductId(Long productId) {
    this.productId = productId;
    return this;
  }

  public AlertConditionModelBuilder withQuantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  public AlertConditionModelBuilder withPharmacyLocationModel(BranchModel branchModel) {
    this.branchModel = branchModel;
    return this;
  }

  public AlertConditionModelBuilder withAlertConditionType(AlertConditionTypeModel alertConditionType) {
    this.alertConditionType = alertConditionType;
    return this;
  }

  public AlertConditionModelBuilder withProduct(ProductModel productModel) {
    this.productModel = productModel;
    return this;
  }

  public AlertConditionModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public AlertConditionModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public AlertConditionModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public AlertConditionModelBuilder withCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public AlertConditionModelBuilder withUpdatedBy(Long updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

  public AlertConditionModel build() {
    AlertConditionModel alertConditionModel = new AlertConditionModel();
    alertConditionModel.setAlertConditionTypeId(alertConditionTypeId);
    alertConditionModel.setPharmacyLocationId(pharmacyLocationId);
    alertConditionModel.setProductId(productId);
    alertConditionModel.setQuantity(quantity);
    alertConditionModel.setPharmacyLocationModel(branchModel);
    alertConditionModel.setAlertConditionType(alertConditionType);
    alertConditionModel.setProduct(productModel);
    alertConditionModel.setId(id);
    alertConditionModel.setCreatedDate(createdDate);
    alertConditionModel.setUpdatedDate(updatedDate);
    alertConditionModel.setCreatedBy(createdBy);
    alertConditionModel.setUpdatedBy(updatedBy);
    return alertConditionModel;
  }
}
