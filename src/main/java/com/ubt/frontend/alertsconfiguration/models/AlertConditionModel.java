/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 06/08/2021.
 */

package com.ubt.frontend.alertsconfiguration.models;

import com.ubt.frontend.commons.models.AlertConditionTypeModel;
import com.ubt.frontend.commons.models.BoBaseModel;
import com.ubt.frontend.commons.models.BranchModel;
import com.ubt.frontend.manageproducts.models.ProductModel;

public class AlertConditionModel extends BoBaseModel {
  private Long alertConditionTypeId;
  private Long pharmacyLocationId;
  private Long productId;
  private Integer quantity;
  private BranchModel pharmacyLocationModel;
  private AlertConditionTypeModel alertConditionType;
  private ProductModel product;

  public ProductModel getProduct() {
    return product;
  }

  public void setProduct(ProductModel productModel) {
    this.product = productModel;
  }

  public Long getPharmacyLocationId() {
    return pharmacyLocationId;
  }

  public void setPharmacyLocationId(Long pharmacyLocationId) {
    this.pharmacyLocationId = pharmacyLocationId;
  }

  public BranchModel getPharmacyLocationModel() {
    return pharmacyLocationModel;
  }

  public void setPharmacyLocationModel(BranchModel branchModel) {
    this.pharmacyLocationModel = branchModel;
  }

  public AlertConditionTypeModel getAlertConditionType() {
    return alertConditionType;
  }

  public void setAlertConditionType(AlertConditionTypeModel alertConditionType) {
    this.alertConditionType = alertConditionType;
  }

  public Long getAlertConditionTypeId() {
    return alertConditionTypeId;
  }

  public void setAlertConditionTypeId(Long alertConditionTypeId) {
    this.alertConditionTypeId = alertConditionTypeId;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }
}
