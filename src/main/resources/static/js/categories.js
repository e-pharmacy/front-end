function readCategories(pageNumber) {
  const name = document.getElementById('name').value;
  $.ajax({
    type: "GET",
    url: "/categories/elements",
    data: {
      name: name,
      page: pageNumber
    },
    success: function (response) {
      $("#categories-table").html(response);
      $("#error-message").text("");
    },
    error: function (e) {
      $("#error-message").text("This product doesn't exists or doesn't have any on stock!");
    }
  });
}

function refreshCategories() {
  document.getElementById('name').value = '';
  $.ajax({
    type: "GET",
    url: "/categories/elements",
    data: {
    },
    success: function (response) {
      $("#categories-table").html(response);
      $("#error-message").text("");
    },
    error: function (e) {
      $("#error-message").text("This product doesn't exists or doesn't have any on stock!");
    }
  });
}