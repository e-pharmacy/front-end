function readProducts(pageNumber) {
    const productId = document.getElementById('product').value;
    const typeId = document.getElementById('type').value;
    const drugId = document.getElementById('drug').value;

    $.ajax({
        type: "GET",
        url: "/products/elements",
        data: {
            productId: productId,
            typeId: typeId,
            drugId: drugId,
            page: pageNumber
        },
        success: function (response) {
            $("#products-table").html(response);
            $("#error-message").text("");
        },
        error: function (e) {
            $("#error-message").text("This product doesn't exists or doesn't have any on stock!");
        }
    });
}

function refreshProducts() {
    $("#product").val(null).trigger("change");
    $("#type").val(null).trigger("change");
    $("#drug").val(null).trigger("change");
    $.ajax({
        type: "GET",
        url: "/products/elements",
        data: {},
        success: function (response) {
            $("#products-table").html(response);
            $("#error-message").text("");
        },
        error: function (e) {
            $("#error-message").text("This product doesn't exists or doesn't have any on stock!");
        }
    });
}