function readAlertsConfigurations(pageNumber) {
  const productId = document.getElementById('product').value;
  const alertTypeId = document.getElementById('alertType').value;
  $.ajax({
    type: "GET",
    url: "/alerts/elements",
    data: {
      productId: productId,
      alertTypeId: alertTypeId,
      page: pageNumber
    },
    success: function (response) {
      $("#alerts-table").html(response);
      $("#error-message").text("");
    }
  });
}

function refreshAlertsConfigurations() {
  $("#product").val(null).trigger("change");
  $("#alertType").val(null).trigger("change");
  $.ajax({
    type: "GET",
    url: "/alerts/elements",
    data: {
    },
    success: function (response) {
      $("#alerts-table").html(response);
      $("#error-message").text("");
    }
  });
}