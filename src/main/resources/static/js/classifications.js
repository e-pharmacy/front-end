function readClassifications(pageNumber) {
  const name = document.getElementById('name').value;
  $.ajax({
    type: "GET",
    url: "/classifications/elements",
    data: {
      name: name,
      page: pageNumber
    },
    success: function (response) {
      $("#classifications-table").html(response);
      $("#error-message").text("");
    },
    error: function (e) {
      $("#error-message").text("This product doesn't exists or doesn't have any on stock!");
    }
  });
}

function refreshClassifications() {
  document.getElementById('name').value = '';
  $.ajax({
    type: "GET",
    url: "/classifications/elements",
    data: {
    },
    success: function (response) {
      $("#classifications-table").html(response);
      $("#error-message").text("");
    },
    error: function (e) {
      $("#error-message").text("This product doesn't exists or doesn't have any on stock!");
    }
  });
}