function readDrugs(pageNumber) {
  const name = document.getElementById('name').value;
  const classificationId = document.getElementById('classification').value;
  const categoryId = document.getElementById('category').value;
  $.ajax({
    type: "GET",
    url: "/drugs/elements",
    data: {
      name: name,
      classificationId: classificationId,
      categoryId: categoryId,
      page: pageNumber
    },
    success: function (response) {
      $("#drugs-table").html(response);
      $("#error-message").text("");
    },
    error: function (e) {
      $("#error-message").text("This product doesn't exists or doesn't have any on stock!");
    }
  });
}

function refreshDrugs() {
  document.getElementById('name').value = '';
  $("#classification").val(null).trigger("change");
  $("#category").val(null).trigger("change");
  $.ajax({
    type: "GET",
    url: "/drugs/elements",
    data: {
    },
    success: function (response) {
      $("#drugs-table").html(response);
      $("#error-message").text("");
    },
    error: function (e) {
      $("#error-message").text("This product doesn't exists or doesn't have any on stock!");
    }
  });
}