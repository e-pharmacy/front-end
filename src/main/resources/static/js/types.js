function readTypes(pageNumber) {
  const name = document.getElementById('name').value;
  $.ajax({
    type: "GET",
    url: "/types/elements",
    data: {
      name: name,
      page: pageNumber
    },
    success: function (response) {
      $("#types-table").html(response);
      $("#error-message").text("");
    },
    error: function (e) {
      $("#error-message").text("This product doesn't exists or doesn't have any on stock!");
    }
  });
}

function refreshTypes() {
  document.getElementById('name').value = '';
  $.ajax({
    type: "GET",
    url: "/types/elements",
    data: {
    },
    success: function (response) {
      $("#types-table").html(response);
      $("#error-message").text("");
    },
    error: function (e) {
      $("#error-message").text("This product doesn't exists or doesn't have any on stock!");
    }
  });
}