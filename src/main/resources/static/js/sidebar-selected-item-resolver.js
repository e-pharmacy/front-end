const pathUrl = window.location.href;
let i = 1;

while (document.getElementById("menu-item-" + i) !== undefined) {
    let menuItem = document.getElementById("menu-item-" + i);
    let menuItemAnchorNode = menuItem.getElementsByTagName('a')[0];

    if (pathUrl.startsWith(menuItemAnchorNode.href)) {
        menuItemAnchorNode.className += " active";
        break;
    }
    i++;
}