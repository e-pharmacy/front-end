function readSideEffects(pageNumber) {
  const name = document.getElementById('name').value;
  $.ajax({
    type: "GET",
    url: "/side-effects/elements",
    data: {
      name: name,
      page: pageNumber
    },
    success: function (response) {
      $("#side-effects-table").html(response);
      $("#error-message").text("");
    },
    error: function (e) {
      $("#error-message").text("This product doesn't exists or doesn't have any on stock!");
    }
  });
}

function refreshSideEffects() {
  cdocument.getElementById('name').value = '';
  $.ajax({
    type: "GET",
    url: "/side-effects/elements",
    data: {
    },
    success: function (response) {
      $("#side-effects-table").html(response);
      $("#error-message").text("");
    },
    error: function (e) {
      $("#error-message").text("This product doesn't exists or doesn't have any on stock!");
    }
  });
}