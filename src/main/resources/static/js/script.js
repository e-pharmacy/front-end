function redirect(url) {
    window.location.replace(url);
}

/**
 * This function is called by a row table that passes the id of that object. It sets the onclick action to the delete
 * button of modal with the provided id and the function that removes the row by id.
 * @param modalId
 * @param objectId
 * @param url
 */
function handleDeleteModal(modalId, objectId, url) {
    document.getElementById("delete").setAttribute("onclick", "removeListElement(" + objectId + "," + url + ")");
    openModal(modalId);
}

/**
 * Removes row by id
 * @param rowId
 * @param url
 */
function removeListElement(rowId, url) {
    if (url != null) {
        $.ajax({
            type: "DELETE",
            url: url + rowId,
            success: function (response) {
            },
            error: function (e) {
                if (e.status) {
                    openDeleteErrorModal();
                } else {
                    //handle other error messages...
                }
            }
        });
    }

    const row = document.getElementById(rowId);
    row.parentNode.removeChild(row);
}

/**
 * Common function for autocomplete, it gets results from specified url
 * and it displays them on the element with the specified id
 * @param elementId
 * @param url
 * @param multiselect
 * @param preSelected
 */
function autocomplete(elementId, url, multiselect, preSelected) {
    var element = $('#' + elementId);
    element.select2({
        theme: "bootstrap4",
        multiple: multiselect,
        allowClear: multiselect,

        ajax: {
            type: "GET",
            url: url + "/autocomplete",
            dataType: 'json',
            delay: 250,
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        }
    });

    if (preSelected != null) {
        for (let i = 0; i < preSelected.length; i++) {
            var option = new Option(preSelected[i].text, preSelected[i].id, true, true);
            element.append(option).trigger('change');
        }
    }
}

function openModal(modalId) {
    $('#' + modalId).modal('show');
}

/**
 * This modal is called when there was an error response code on delete request.
 * It hides the popup delete confirmation modal and shows the error modal message.
 */
function openDeleteErrorModal() {
    $('#modalDeleteItem').modal('hide');
    $('#modalErrorItem').modal('show');
}

function imagePreview() {
    $(function () {
        $(document).on("change", ".uploadFile", function () {
            var uploadFile = $(this);
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

            if (/^image/.test(files[0].type)) { // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file

                reader.onloadend = function () { // set image data as background of div
                    //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                    $('.imagePreview').attr("src", this.result);
                }
            }

        });
    });
    // var image = $('.imagePreview');
    // if (image.attr('src') == null) {
    //     image.attr('src', './images/no-image.png')
    // }
}

function updateBranch() {
    //TODO: implement it...
}

//Active default branch on pageload
function openNotifications() {
    $('#notification-button').dropdown();

    $.ajax({
        type: "GET",
        url: "/notifications/dropdown",
        data: {},
        success: function (response) {
            $("#notifications-list").html(response);
            $("#error-message").text("");
        },
        error: function (e) {
            $("#error-message").text("");
        }
    });
}

/**
 * Formats the number with commas as thousands separators.
 * @param x number to be formatted.
 * @returns {string}
 */
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}