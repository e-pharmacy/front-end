function readSupplies(pageNumber) {
    let productId = document.getElementById('product').value;
    let branchId = document.getElementById('branch').value;


    $.ajax({
        type: "GET",
        url: "/supplies/elements",
        data: {
            page: pageNumber,
            productId: productId,
            branchId: branchId
        },
        success: function (response) {
            $("#supplies-table").html(response);
        },
        error: function (e) {
        }
    });
}

function refreshSupplies() {
    $("#product").val(null).trigger("change");
    $("#branch").val(null).trigger("change");
    $.ajax({
        type: "GET",
        url: "/supplies/elements",
        data: {},
        success: function (response) {
            $("#supplies-table").html(response);
        },
        error: function (e) {
        }
    });
}