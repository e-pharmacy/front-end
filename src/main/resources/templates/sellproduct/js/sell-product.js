function readProduct(barcode, quantity) {
    if (!isValidScanProductInput(barcode, quantity)) {
        return;
    }

    let alreadyAddedQuantity = $("#" + barcode + 'quantity').text();
    let productPricePerUnit = $("#" + barcode + 'pricePerUnit').text();

    $.ajax({
        type: "GET",
        url: "/products",
        data: {
            barcode: barcode,
            quantity: quantity,
            alreadyAdded: alreadyAddedQuantity,
            fragmentName: "product_list_item::product_item"
        },
        success: function (response) {
            if ($("#" + barcode).length) {
                let totalQuantity = Number(quantity) + Number(alreadyAddedQuantity)
                let price = totalQuantity * Number(productPricePerUnit);
                let tax = price * 10 / 100; //10% of total price

                $("#" + barcode + 'quantity').text(totalQuantity);
                $("#" + barcode + 'price').text(price);
                $("#" + barcode + 'taxesPrice').text(tax);
                $("#" + barcode + 'subTotal').text(numberWithCommas(price + tax));
            } else {
                $("#products-table-body").append(response);
            }

            //Add total price to the digital total price input
            let latestItemSubTotalPrice = $("#" + barcode + 'subTotal').text();
            $('#totalPrice').text(latestItemSubTotalPrice);

            handleErrorMessageDisplay();
        },
        error: function (e) {
            if (e.status === 404) {
                handleErrorMessageDisplay('Cannot find product! There is no product on stock with the provided barcode.');
            } else if (e.status === 400) {
                handleErrorMessageDisplay(e.responseJSON.message);
                // handleErrorMessageDisplay('Insufficient stock supplies! There are not enough items on stock for this product.');
            } else {
                handleErrorMessageDisplay(e.responseJSON.message);
            }
        }
    });

    $.ajax({
        type: "GET",
        url: "/products",
        data: {
            barcode: barcode,
            quantity: quantity,
            fragmentName: "product_details"
        },
        success: function (response) {
            $("#product-details").html(response);
        }
    });
}

/**
 * It shows or hide the error message based on message text.
 * If message is not provided, it removes the message text and makes it invisible and vice-versa.
 * @param message
 */
function handleErrorMessageDisplay(message) {
    let errorMessage = $("#error-message");

    if (!message) {
        errorMessage.text("");
        errorMessage.css("visibility", "hidden");
    } else {
        errorMessage.text(message);
        errorMessage.css("visibility", "visible");
    }
}

function isValidScanProductInput(barcode, quantity) {
    if (barcode === undefined || barcode <= 0 || Number(barcode) % 1 !== 0) {
        $("#error-message").text();
        handleErrorMessageDisplay('Invalid barcode number!');
        return false;
    } else if (quantity === undefined || quantity <= 0 || Number(quantity) % 1 !== 0) {
        handleErrorMessageDisplay('Invalid quantity number!');
        return false;
    }
    return true;
}

function sellProducts() {
    let tableRows = document.getElementById("sell-products-table").rows;

    let products = [];
    for (let i = 1; i < tableRows.length; i++) {
        let productObject = {
            "barcode": tableRows[i].id,
            "quantity": tableRows[i].cells[1].children[0].innerHTML
        };

        products.push(productObject);
    }

    $.ajax({
        type: "POST",
        contentType: "application/json;charset=utf-8",
        url: "/products/sell",
        data: JSON.stringify(products),
        success: function () {
            $('#' + "modalInfoItem").modal('show');
        },
        error: function () {
        }
    });
}