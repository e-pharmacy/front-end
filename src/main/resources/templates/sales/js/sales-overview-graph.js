/**
 * Line chart initialization
 * @type {*|jQuery|HTMLElement}
 */

'use strict';

//Variables
const $lineChart = $('#chart-sales-dark');

let lineChartLabelsArray = [];
/*<![CDATA[*/
/*[# th:each="page : ${overviewChart}"]*/
lineChartLabelsArray.push(/*[[${page.month}]]*/);
/*[/]*/
/*]]>*/

let lineChartDataArray = [];
/*<![CDATA[*/
/*[# th:each="page : ${overviewChart}"]*/
lineChartDataArray.push(/*[[${page.profit}]]*/);
/*[/]*/
/*]]>*/


//Methods
function init() {
    let salesChart = new Chart($lineChart, {
        type: 'line',
        options: {
            scales: {
                yAxes: [{
                    gridLines: {
                        lineWidth: 1,
                        color: Charts.colors.gray[900],
                        zeroLineColor: Charts.colors.gray[900]
                    },
                    ticks: {
                        callback: function (value) {
                            if (!(value % 10)) {
                                return '€' + numberWithCommas(value);
                            }
                        }
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function (item, data) {
                        var label = data.datasets[item.datasetIndex].label || '';
                        var yLabel = item.yLabel;
                        var content = '';

                        if (data.datasets.length > 1) {
                            content += '<span class="popover-body-label mr-auto">' + label + '</span>';
                        }

                        content += '€' + numberWithCommas(Number(yLabel));
                        return content;
                    }
                }
            }
        },
        data: {
            labels: lineChartLabelsArray,
            datasets: [{
                label: 'Performance',
                data: lineChartDataArray,
            }]
        }
    });

    //Save to jQuery object
    $lineChart.data('chart', salesChart);
}

//Events
init();