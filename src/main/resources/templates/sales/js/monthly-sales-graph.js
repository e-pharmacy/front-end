/**
 * Bars chart initialization
 * @type {*|jQuery|HTMLElement}
 */

'use strict';

// Variables
const $barsChart = $('#chart-bars');

let labelsArray = [];
/*<![CDATA[*/
/*[# th:each="page : ${monthlySalesGraph}"]*/
labelsArray.push(/*[[${page.month}]]*/);
/*[/]*/
/*]]>*/

let dataArray = [];
/*<![CDATA[*/
/*[# th:each="page : ${monthlySalesGraph}"]*/
dataArray.push(/*[[${page.salesCount}]]*/);
/*[/]*/
/*]]>*/

// Methods
function init() {
    let ordersChart = new Chart($barsChart, {
        type: 'bar',
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        callback: function (value) {
                            if (!(value % 10)) {
                                return numberWithCommas(value);
                            }
                        }
                    }
                }]
            },
            tooltips: {
                callbacks: {
                    label: function (item) {
                        const yLabel = item.yLabel;
                        return numberWithCommas(Number(yLabel));
                    }
                }
            }
        },
        data: {
            labels: labelsArray,
            datasets: [{
                label: 'Sales',
                data: dataArray
            }]
        }
    });

    // Save to jQuery object
    $barsChart.data('chart', ordersChart);
}

// Init chart
init();